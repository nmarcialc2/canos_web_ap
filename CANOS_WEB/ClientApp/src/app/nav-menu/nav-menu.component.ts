import { Component } from '@angular/core';
import { AuthGuard } from '../shared/services/auth-guard.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})

export class NavMenuComponent {
  isExpanded = false;
  constructor(public authGuard: AuthGuard) {

  }
  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
  logOut() {
    localStorage.removeItem("jwt");
  }
}
