import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { RepositoryService } from '../shared/services/repository.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})

export class UserLoginComponent implements OnInit {

  invalidLogin: boolean;




  constructor(private router: Router, private http: HttpClient, private repo: RepositoryService) {

  }

  ngOnInit() {
  }

  login(form: NgForm) {

    let credentials = JSON.stringify(form.value);

    this.repo.getDataBody("api/auth/login", credentials).subscribe(response => {

      let token = (<any>response).token;

      localStorage.setItem("jwt", token);

      this.invalidLogin = false;

      this.router.navigate(["/Dashboard"]);

    }, err => {

      this.invalidLogin = true;

    });

  }



}
